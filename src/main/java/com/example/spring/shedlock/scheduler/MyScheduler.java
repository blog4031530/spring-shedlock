package com.example.spring.shedlock.scheduler;

import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
@Slf4j
public class MyScheduler {

	@Scheduled(cron = "0 * * * * *")
	@SchedulerLock(name = "perMinuteScheduler", lockAtLeastFor = "50s", lockAtMostFor = "55s")
	public void task1() {
		log.info("task1 run");
	}

	@Scheduled(cron = "0 * * * * *")
	@SchedulerLock(name = "perMinuteScheduler", lockAtLeastFor = "50s", lockAtMostFor = "55s")
	public void task2() {
		log.info("task2 run");
	}
}
